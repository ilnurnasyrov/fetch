module Fetch
  class Error < StandardError
    attr_reader :original_exception, :object, :path

    def initialize(original_exception:, object:, path:)
      @original_exception = original_exception
      @object = object
      @path = path
    end
  end

  def context
    {
      original_exception: @original_exception,
      object: @object,
      path: @path
    }
  end

  def raven_context
    { extra: { exception_context: context } }
  end
end
