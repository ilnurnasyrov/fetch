require "fetch/version"
require "fetch/error"

module Fetch
  extend self

  def fetch(object, *path)
    value = object

    for key in path
      value = value.fetch(key)
    end

    value
  rescue KeyError, IndexError => exception
    raise Fetch::Error.new(
      original_exception: exception,
      object: object,
      path: path
    )
  end
end

def Fetch(*options)
  Fetch.fetch(*options)
end
