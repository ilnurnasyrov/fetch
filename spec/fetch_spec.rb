RSpec.describe Fetch do
  it "has a version number" do
    expect(Fetch::VERSION).not_to be nil
  end

  it "fetches value from first nesting level of hash" do
    expect(
      Fetch(
        { first: :value },
        :first
      )
    ).to eq :value
  end

  it "fetches value from first nesting level of array" do
    expect(
      Fetch(
        [:value],
        0
      )
    ).to eq :value
  end

  it "fetches value from second level of hash" do
    expect(
      Fetch(
        { first: { second: :value }},
        :first,
        :second
      )
    ).to eq :value
  end

  it "fetches value from second level of array" do
    expect(
      Fetch(
        [first: :value],
        0,
        :first
      )
    ).to eq :value
  end

  it "raises Fetch::Error with IndexError as original_exception" do
    begin
      Fetch([], 1)
    rescue => e
    end

    expect(e).to be_instance_of Fetch::Error
    expect(e.object).to eq []
    expect(e.path).to eq [1]
    expect(e.original_exception.class).to eq IndexError
    expect(e.original_exception.message).to eq "index 1 outside of array bounds: 0...0"
  end

  it "raises Fetch::Error with KeyError as original_exception" do
    begin
      Fetch({}, 1)
    rescue => e
    end

    expect(e).to be_instance_of Fetch::Error
    expect(e.object).to eq({})
    expect(e.path).to eq [1]
    expect(e.original_exception.class).to eq KeyError
    expect(e.original_exception.message).to eq "key not found: 1"
  end
end
