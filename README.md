# Fetch

Fetch() for deep fetching

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'fetch'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fetch

## Usage

Fetch({key: [:value]}, :key, 0) #=> :value
